<?php
/***************************************
  * http://www.program-o.com
  * PROGRAM O
  * Version: 2.6.3
  * FILE: index.php
  * AUTHOR: Elizabeth Perreau and Dave Morton
  * DATE: FEB 01 2016
  * DETAILS: This is the interface for the Program O JSON API
  ***************************************/
  $cookie_name = 'Program_O_JSON_GUI';
  $botId = filter_input(INPUT_GET, 'bot_id');
  $convo_id = (isset($_COOKIE[$cookie_name])) ? $_COOKIE[$cookie_name] : jq_get_convo_id();
  $bot_id = (isset($_COOKIE['bot_id'])) ? $_COOKIE['bot_id'] :($botId !== false && $botId !== null) ? $botId : 1;
  if (is_nan($bot_id) || empty($bot_id)) $bot_id = 1;
  setcookie('bot_id', $bot_id);
  // Experimental code
  $base_URL  = 'http://' . $_SERVER['HTTP_HOST'] . '/';                                   // set domain name for the script
  $this_path = str_replace(DIRECTORY_SEPARATOR, '/', realpath(dirname(__FILE__)));  // The current location of this file, normalized to use forward slashes
  $this_path = str_replace($_SERVER['DOCUMENT_ROOT'], $base_URL, $this_path);       // transform it from a file path to a URL
  $url = str_replace('gui/jquery', 'chatbot/conversation_start.php', $this_path);   // and set it to the correct script location
/*
  Example URL's for use with the chatbot API
  $url = 'http://api.program-o.com/v2.3.1/chatbot/';
  $url = 'http://localhost/Program-O/Program-O/chatbot/conversation_start.php';
  $url = 'chat.php';
*/


  $display = '';

  /**
   * Function jq_get_convo_id
   *
   *
   * @return string
   */
  function jq_get_convo_id()
  {
    global $cookie_name;
    session_name($cookie_name);
    session_start();
    $convo_id = session_id();
    session_destroy();
    setcookie($cookie_name, $convo_id);
    return $convo_id;
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="main.css" media="all" />
    <link rel="icon" href="./favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Program O jQuery GUI Examples</title>
    <meta name="Description" content="A Free Open Source AIML PHP MySQL Chatbot called Program-O. Version2" />
    <meta name="keywords" content="Open Source, AIML, PHP, MySQL, Chatbot, Program-O, Version2" />
    <style type="text/css">
      h3 {
        text-align: center;
      }
      hr {
        width: 80%;
        color: green;
        margin-left: 0;
      }

      .user_name {
        color: rgb(16, 45, 178);
      }
      .bot_name {
        color: rgb(204, 0, 0);
      }
      #urlwarning {
        right: auto;
        left: 10px;
        width: 50%;
        font-size: large;
        font-weight: bold;
        background-color: white;
      }
      .leftside {
        text-align: right;
        float: left;
        width: 48%;
      }
      .rightside {
        text-align: left;
        float: right;
        width: 48%;
      }
      .centerthis {
        width: 90%;
      }
      #chatdiv {
        margin-top: 20px;
        text-align: center;
        width: 100%;
      }
      p.center {
        text-align: center;
      }
      hr.center {
        margin: 0 auto;
      }
      #convo_id {
        position: absolute;
        top: 10px;
        right: 10px;
        border: 1px solid red;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-shadow: 2px 2px 2px 0 #808080;
        padding: 5px;
        border-radius: 5px;
      }

    </style>
  </head>
  <body>

    <div class="header">
        <img src="images/logo.svg">
    </div>

    <div class="section-bot">
        <div id="syfybot" class="centerthis">
            <canvas class="emscripten" id="canvas" oncontextmenu="event.preventDefault()" height="400px" width="400px"></canvas>
        </div>
    </div>

    <div class="centerthis">
      <div class="rightside">
      <div class="botspeech">
        <div  class="triangle-border bottom blue">
          <div class="botsay">Hey!</div>
        </div>
      </div>

      </div>
      <div class="leftside">
      <div class="userspeech">
        <div  class="triangle-border-right bottom color-gray">
          <div class="usersay">&nbsp;</div>
        </div>
      </div><br />

      </div>
    </div>
    <div class="clearthis"></div>
    <div class="centerthis">
      <form method="post" name="talkform" id="talkform" action="index.php">
        <div id="chatdiv">
          <label for="submit"></label>
          <input type="text" name="say" id="say" size="60"/>
          <input type="submit" name="submit" id="submit" class="submit"  value="say" />
          <input type="hidden" name="convo_id" id="convo_id" value="<?php echo $convo_id;?>" />
          <input type="hidden" name="bot_id" id="bot_id" value="<?php echo $bot_id;?>" />
          <input type="hidden" name="format" id="format" value="json" />
        </div>
      </form>
    </div>
    <div id="urlwarning"><?php echo $display ?></div>

    <div class="askthis centerthis">
        <p>Examples:</p>

        <p>WHEN WILL BE THE DOOMSDAY?</p>
        <p>WHAT IS WARP SPEED?</p>
        <p>HOW FAR IS ALPHA CENTAURI?</p>
        <p>WHAT IS STAR WARS?</p>

        <p>Add more phrases in /aiml as .aiml format and upload them via admin interface</p>
    </div>



    <script type="text/javascript" src="jquery-1.9.1.min.js"></script>
    <script src="webgl/libs/three.min.js"></script>
    <script src="webgl/controls/OrbitControls.js"></script>
    <script src="webgl/loaders/ColladaLoader.js"></script>
    <script src="webgl/loaders/OBJLoader.js"></script>

    <script src="webgl/FileImporter.js"></script>
    <script src="webgl/Main.js"></script>

    <script type="text/javascript" >
     $(document).ready(function() {
      // put all your jQuery goodness in here.
        $('#talkform').submit(function(e) {

          var message = $('#say').val();
          console.log('onMessage:', message);
          SendMessage('receiver','onMessage', message);

          e.preventDefault();
          var user = $('#say').val();
          $('.usersay').text(user);
          var formdata = $("#talkform").serialize();
          $('#say').val('');
          $('#say').focus();
          $.get('<?php echo $url ?>', formdata, function(data){
            var b = data.botsay;
            if (b.indexOf('[img]') >= 0) {
              b = showImg(b);
            }
            if (b.indexOf('[link') >= 0) {
              b = makeLink(b);
            }
            var usersay = data.usersay;
            if (user != usersay) {
              $('.usersay').text(usersay);
            }
            $('.botsay').html(b);
          }, 'json').fail(function(xhr, textStatus, errorThrown){
            $('#urlwarning').html("Something went wrong! Error = " + errorThrown);
          });
          return false;
        });
      });
      function showImg(input) {
        var regEx = /\[img\](.*?)\[\/img\]/;
        var repl = '<br><a href="$1" target="_blank"><img src="$1" alt="$1" width="150" /></a>';
        var out = input.replace(regEx, repl);
        console.log('out = ' + out);
        return out
      }
      function makeLink(input) {
        var regEx = /\[link=(.*?)\](.*?)\[\/link\]/;
        var repl = '<a href="$1" target="_blank">$2</a>';
        var out = input.replace(regEx, repl);
        console.log('out = ' + out);
        return out;
      }

        initUnity();
       function initUnity() {
            console.log('initUnity');
            var script = document.createElement('script');

            //unity init data
            Module = {
                doNotCaptureKeyboard: true,
                TOTAL_MEMORY: 268435456,
                errorhandler: onerror,
                compatibilitycheck: null,
                dataUrl: "unity/Release/unity.data",
                codeUrl: "unity/Release/unity.js",
                memUrl: "unity/Release/unity.mem",
            };

            script.setAttribute('src', 'unity/Release/UnityLoader.js');
            document.body.appendChild(script);
        }

        function onerror(err, url, line) {
            console.log(err);
            new webglFallback();
            return true;
        }

    </script>
  </body>
</html>
