var FileImporter = {
  prevButton: null,
  dae: null,

  loadDAE: function(path) {


    var loader = new THREE.ColladaLoader();
    loader.options.convertUpAxis = true;

    console.log('path', path);

    loader.load(path, function(collada) {


      dae = collada.scene;

      scene.add(dae);

      dae.scale.x = dae.scale.y = dae.scale.z = 0.2;
      dae.position.y = 12;
      //dae.updateMatrix();

    });

  },

  loadOBJ: function(path) {
    var manager = new THREE.LoadingManager();



    var loader = new THREE.OBJLoader( manager );
    loader.load( path, function ( object ) {

      /*
      object.traverse( function ( child ) {
        if ( child instanceof THREE.Mesh ) {
          child.material.map = texture;
        }
      } );
      */

      
      scene.add( object );

    });
  }

};