var Main = {
  directionForward: false,
  camera: null,
  light: null,
  speakLight: null,
  scene: null,
  renderer: null,
  container: null,
  controls: null,
  clock: new THREE.Clock(),
  mixers: [],

  init: function() {
    //if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

    //container = document.createElement( 'div' );
    container = $('#syfybot');
    //document.body.appendChild( container );

    camera = new THREE.PerspectiveCamera( 45, container.width() / container.height(), 1, 2000 );
    scene = new THREE.Scene();

    // grid
    var gridHelper = new THREE.GridHelper( 14, 28, 0x303030, 0x303030 );
    gridHelper.position.set( 0, - 0.04, 0 );
    //scene.add( gridHelper );

    //lights
    light = new THREE.PointLight(0xffffff, 10);
    light.position.set(0, 0, 50);
    scene.add(light);

    speakLight = new THREE.PointLight(0x00ff00, 0);
    speakLight.position.set(0, 0, 40);
    scene.add(speakLight);

    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( container.width(), container.height() );
    renderer.setClearColor( 0x202020 );
    container.append( renderer.domElement );

    // controls, camera
    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.target.set( 0, 12, 0 );
    camera.position.set( 2, 18, 28 );
    controls.update();

    window.addEventListener( 'resize', Main.onWindowResize, false );

    Main.animate();
  },

  onWindowResize:function () {
    /*
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
    */
  },

  animate:function () {
    requestAnimationFrame( Main.animate );
    if ( Main.mixers.length > 0 ) {
      for ( var i = 0; i < Main.mixers.length; i ++ ) {
        Main.mixers[ i ].update( clock.getDelta() );
      }
    }

    Main.render();
  },

  render:function () {
    renderer.render( scene, camera );
  }
};

$(document).ready(function() {
  //Main.init();
  //FileImporter.loadDAE('./webgl/models/atlas.dae');
  //FileImporter.loadDAE('http://dev.ufomammoot.com/syfy/gui/jquery/webgl/models/atlas.DAE');
});