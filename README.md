This project is based on:
https://github.com/Program-O/Program-O

if you want to fiddle around and test with a clean installation in a new folder, follow this guide:
https://github.com/Program-O/Program-O/wiki/Installation-Guide#administer-the-chatbot

This project currently writes into a database called "syfybot".
You can edit the database name in config/global_config.php
(You can use XAMPP to install a database for your dev environment)

The current knowledgebase of the bot is located in the /aiml folder.
Add new AIML behaviors here and upload them via the admin interface.
You can reach the admin interface by appending /admin to your base url (.../syfybot/admin).
user: btree
pw: btree
editable in /config/global_config.php

AIML Overview
http://www.alicebot.org/documentation/
http://www.pandorabots.com/pandora/pics/wallaceaimltutorial.html

Symbolic Recursion
http://www.alicebot.org/documentation/srai.html
